$(function () {
  $('[data-toggle="popover"]').popover()
  $('[data-toggle="tooltip"]').tooltip()

});
$('.carousel').carousel({
  interval: 3000
});

$('#exampleModal').on('show.bs.modal', function (e) {
  console.log('se va a mostrar el modal')

  $('#' + e.relatedTarget.id).removeClass('btn-outline-success')
  $("#" + e.relatedTarget.id).addClass('btn-danger')
  $("#" + e.relatedTarget.id).prop('disabled', true)
});

$('#exampleModal').on('shown.bs.modal', function (e) {
  console.log('se mostró el modal')
});

$('#exampleModal').on('hide.bs.modal', function (e) {
  console.log('se oculta el modal')
  
  $(".btnModal").removeClass('btn-danger')

  $(".btnModal").prop('disabled', false)
});

$('#exampleModal').on('hidden.bs.modal', function (e) {
  console.log('se ocultó el modal')
});

